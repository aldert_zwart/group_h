'''
The module bicycle contains the BicycleData Class that allows the
user to perform data analysis from a dataset.
'''

from zipfile import ZipFile
import datetime as dt
import warnings
import requests
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sn
import numpy as np

warnings.filterwarnings("ignore")


class BicycleData:
    '''
    The Class BicycleData is able to not only download and open the
    CSV file containing the dataset, but also to plot a correlation
    matrix and plotting weekly data.

    Attributes
    ----------
        download_status : NoneType initially, str if download_zip
        method is called.

        data_frame : NoneType initially, pandasdataframe when the csv file 
        is unpacked.

        open_csv_statement : NoneType initially, str when a 
        error occurs.

    Methods
    ----------
        download_zip : checks if the .zip documents exists in the
        downloads directory and downloads if inexistent. 

        open_csv : unzips the hours.csv file in the downloads directory.
        
        plot_corr_matrix : plots correlation matrix for the variables:
        month, humidity, weather situation, temperature, windspeed, 
        and total number of bike rentals.

        week_plotting : plots the distribution of number, 
        of cyclists in a given week chosen by the user.
        
        plot_bar_chart : plots the average of the total rentals by month

        forecast : the forecast method will calculate and plot the mean and
        standard deviation for the defined month.
    '''

    def __init__(self, download_status=None, open_csv_status=None, 
                 data_frame=None):
        self.download_status = download_status
        self.open_csv_status = open_csv_status
        self.data_frame = data_frame

    def download_zip(self, url: str):
        '''
        The download_zip method will receive the URL in which the
        .zip file that contains the data can obtained from and it
        will store the file in the computer in the 'Data' folder.
        No download will take place if the file already exists.

        Parameters
        -----------
        url: str The URL of the source of the .zip file.
  
        Returns
        --------
        None
        '''

        try:
            file_open = open('../Downloads/data.zip')
            download_status = 'No download performed.'
            file_open.close()
        except:
            request_zip = requests.get(url)
            with open('../Downloads/data.zip', 'wb') as code:
                code.write(request_zip.content)
            download_status = 'Download successful.'
        finally:
            self.download_status = download_status


    def open_csv(self):
        '''
        The directory "/downloads" is openend and the hour.csv
        file will be unzipped and imported into a pd.DataFrame

        Parameters
        ---------------
        None

        Returns 
        ---------------
        None
        '''
        
        try:
            data = ZipFile("../Downloads/data.zip").extract('hour.csv', path='../Downloads')
            data_frame = pd.read_csv(data)

            #Create auxiliary column 'C' for datetime concatenation and conversion
            data_frame['index'] = ''
            
            for number in range(len(data_frame)):
                if len(str(data_frame['hr'][number])) == 2:
                    data_frame['index'][number] = data_frame['dteday'][number]\
                     + ' ' + str(data_frame['hr'][number])
                else:
                    data_frame['index'][number] = data_frame['dteday'][number]\
                     + ' 0' + str(data_frame['hr'][number])

            data_frame['index'] = pd.to_datetime(data_frame['index'], format='%Y-%m-%d %H')

            data_frame.index = data_frame['index']
            data_frame.drop(columns=['index'], inplace=True)

            open_csv_status = 'csv succesfully opened'
        except:
            raise FileNotFoundError('csv file not found')
        finally:
            self.open_csv_status = open_csv_status
            self.data_frame = data_frame
            
    def plot_corr_matrix(self):
        """
        This method will create a  matrix with month, humidity, 
        weather situation, temperature, windspeed, 
        and total number of bike rentals of initial matrix 'hour' 
        and calculate the correlation of each pair of features.

        Parameters
        ---------------
        None

        Returns 
        ---------------
        None
        """

        try:
            sub_data = {'month':self.data_frame['mnth'],
                        'hum':self.data_frame['hum'],
                        'weathersit':self.data_frame['weathersit'],
                        'temp':self.data_frame['temp'],
                        'windspeed':self.data_frame['windspeed'],
                        'cnt':self.data_frame['cnt']}
            sub_data = pd.DataFrame(sub_data, columns=['month', 'hum', 
                                                       'weathersit', 'temp', 
                                                       'windspeed', 'cnt'])
            corr_matrix = sub_data.corr()
            fig, axe = plt.subplots(figsize=(10, 8))
            mask = np.zeros_like(corr_matrix, dtype=bool)
            mask[np.triu_indices_from(mask)] = True
            sn.heatmap(corr_matrix, mask=mask, cmap='RdYlGn',\
                                    center=0, robust=True, annot=True)
            plt.show()
        except:
            raise FileNotFoundError('csv file could not be found,\
                                    please refer to open_csv method') 
 
    def week_plotting(self):
        """
        The week_plotting method will receive the dataset data_frame,
        then, a new column (weeknum) is created to house the number
        of the week correspondent to each day and hour. The user will
        have the opportunity to choose a week within the range 
        [0;102] to visualize.

        Parameters
        ------------
        None

        Returns
        ------------
        None
        """  
        
        i = 0
        self.data_frame['weeknum'] = 0

        for index, row in self.data_frame.iterrows():
            if row['weekday'] == 0 and row['hr'] == 0:
                i += 1
                self.data_frame.at[index, 'weeknum'] = i
            else:
                self.data_frame.at[index, 'weeknum'] = i

        print('Choose the week to plot')
        chosen_week = int(input())

        if chosen_week in range(0, 102):
            week = self.data_frame.loc[self.data_frame['weeknum'] 
                                       == chosen_week]

            plt.figure(figsize=(14, 5))
            plt.grid(color='grey', linestyle='-', linewidth=0.75, alpha=0.3)
            plt.plot(week.index, week['cnt'], color='darkseagreen')
            plt.title('Number of cyclists per day and hour in week ' 
                      + str(chosen_week))
            plt.xlabel('Date')
            plt.ylabel('Number of cyclists')
        else:
            raise ValueError("The week number should be between 0 and 102")

    def plot_bar_chart(self):
        """
        The plot_bar_chart method will receive the dataset data_frame,
        then, it will calculate the average of total rentals by month of 
        2011 and 2012, after that, calculate (0.5*month_of_2011+0.5*month_of_2012)
        and then show it in a bar chart. 

        Parameters
        ------------
        None

        Returns
        ------------
        None
        """  
        try:
            month = pd.to_datetime(self.data_frame['dteday']).dt.month
            year = pd.to_datetime(self.data_frame['dteday']).dt.year
            month_cnt = pd.DataFrame({'year':year, 'month':month})
            month_cnt['count'] = self.data_frame['cnt']
            avg_groupby_ym = round(month_cnt.groupby(by=['month', 'year']).mean())

            avg_groupby_ym = avg_groupby_ym.rename(columns={'count':'AVG'})
            avg_groupby_m = round(avg_groupby_ym.groupby(by=['month']).mean())

            objects = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                       'Jul', "Aug", 'Sep', 'Oct', 'Nov', 'Dec']
            plt.figure(figsize=(12, 5))

            x_axis = np.arange(avg_groupby_m.shape[0])
            y_axis = avg_groupby_m['AVG']
            plt.grid(color='grey', linestyle='-', linewidth=0.75, alpha=0.3)
            plt.bar(x_axis, y_axis, align='center', color='darkseagreen')
            plt.xticks(x_axis, objects)
            plt.title('Average total rentals by month of 2011 and 2012')
            plt.show()
            
        except:
            raise FileNotFoundError('csv not found, check open_csv method')  
        
    def forecast(self):
        """
        The end-user can input a string or integer in order to define the month. 
        The mean and standard deviation for the defined month 
        will then be calculated in order to present a plot with 
        the mean and standard deviation. 
        
        Parameters
        ------------
        None

        Returns
        ------------
        None        
        """
        month = input("Please select the month:")
        if month.isdigit():
            month = int(month)
        try:
            month == type(int) or type(str)
        except ValueError:
            print("Please select the month as a string or number")
        try:
            if type(month) == int:
                self.data_frame = self.data_frame[self.data_frame.index.month == month]
            elif type(month) == str:
                self.data_frame = self.data_frame[self.data_frame.index.strftime('%B') == month]
        except ValueError:
            print("Please define the right month.")
        try:    
            mean = self.data_frame.groupby([self.data_frame.weekday,\
                                            self.data_frame.hr])['cnt'].mean().to_numpy()
            std = self.data_frame.groupby([self.data_frame.weekday,\
                                           self.data_frame.hr])['cnt'].std().to_numpy()
            high = mean + std
            low = mean - std
            plt.figure(figsize=(14, 5))
            plt.title(f"Expected weekly rentals in \
                                { self.data_frame.index.month_name().unique()[0] }")
            plt.grid(color='grey', linestyle='-', linewidth=0.75, alpha=0.3)
            plt.plot(mean, c='darkgreen', label='mean')
            plt.ylabel("Average count of rented bicycles")
            plt.xlabel('Hours during a week')
            plt.fill_between(list(range(0, len(low))), low, high,\
                                color='darkseagreen', alpha=.4, \
                                label="Standard deviation of 1")
            plt.legend()
            plt.show()
        except:
            exception("An error occurred during the plotting process.")
            