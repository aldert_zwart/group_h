————————————————————————————
MODULE CAPABILITIES 
————————————————————————————

The bicycle.py module contains a Class that is built to help users perform data analysis, throughout all the stages of the process:
- Accessing the file
- Preparing the file and read it
- Curating the data
- Analysing the data (correlation + plot for simple statistics and visualization of trends)
- Implementing a simple forecast method for decision making


Inside the notebook storytelling.ipynb, examples of how the class can fully be used is demonstrated to the user. It is also used to perform some simple data analysis with real data to be able to reach conclusions relevant to the business.  

———————————————————//——————————————————————
————————————————————————————
INSTRUCTIONS FOR CREATING VIRTUAL ENVIRONMENT 
————————————————————————————

Before using the capabilities of the module “bicycle.py”, the user needs to make sure the environment where the application is running has the relevant versions of the libraries used. 

MACINTOSH + LINUX: Open the terminal app.
WINDOWS: Open Anaconda prompt.

Run the following:

conda env create -f bicycle_env.yml

Once the terminal finishes installing the environment, make sure it is activated with the following command:

conda activate bicycle_env

At a later stage, to come back to the original base environment, run the following command:

conda activate base

If, for some reason, the environment is no longer needed, run the following command to delete it:

conda env remove -n bicycle_env
———————————————————//——————————————————————
————————————————————————————
HOW TO USE THE BicycleData CLASS
————————————————————————————

To use the Class, make sure you add the following code to your algorithm:

from bicycle import BicycleData

Then, to initiate the Class, create a variable such as the following:

your_variable = BicycleData()

You are now ready to use the attributes and methods included in the Class. Please check the Class documentation (along with the documentation of the attributes and methods for more details on how to use these).
For this, you can run:

your_variable?

———————————————————//——————————————————————
————————————————————————————
INFO AND AUTHORS 
————————————————————————————

The project was conducted for the course “Advanced Programming for Data Science” at NOVA School of Business and Economics. It will perform data analysis for a bike-sharing service with data from 2011 to 2012.

Authors:
Catarina Balsas | 26024@novasbe.pt
Diogo Pinheiro | 46555@novasbe.pt
Wu Zhen Ze | 44524@novasbe.pt
Aldert Zwart | 44612@novasbe.pt


Carcavelos, 2021
